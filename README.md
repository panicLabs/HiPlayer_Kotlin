# HiPlayer Kotlin Version
A video player which can help users to learn English through videos. 
> **Current Feature:**

> - Play local videos
> - Basic video control, like play, pause, resume, lock
> - Support local subtitle
> - Implement setOnTimedTextListener
