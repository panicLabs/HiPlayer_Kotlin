package id.panislabs.hiplayer

import android.app.Application

import com.birbit.android.jobqueue.JobManager
import com.birbit.android.jobqueue.config.Configuration
import com.nostra13.universalimageloader.core.DisplayImageOptions
import com.nostra13.universalimageloader.core.ImageLoader
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration

/**
 * @author      paniclabs.
 * @created     on 2017/1/14.
 * @email       panic.inc.dev@gmail.com
 */
class MyApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        instance = this

        initImageLoader()
    }

    private fun initImageLoader() {
        val defaultOptions = DisplayImageOptions.Builder()
                                .cacheInMemory(true)
                                .cacheOnDisk(true)
                .build()

        val imageLoaderConfiguration = ImageLoaderConfiguration.Builder(applicationContext)
                .defaultDisplayImageOptions(defaultOptions)
                .build()

        ImageLoader.getInstance().init(imageLoaderConfiguration)
    }

    companion object {
        var instance: MyApplication? = null
            private set
        private var mJobManager: JobManager? = null

        //always keep at least one consumer alive
        //up to 5 consumers at a time
        //5 jobs per consumer
        //.consumerKeepAlive(120)//wait 2 minute
        val jobManager: JobManager
            get() {
                if (mJobManager == null) {
                    val jobConfiguration = Configuration.Builder(instance!!.applicationContext)
                            .minConsumerCount(5)
                            .maxConsumerCount(10)
                            .loadFactor(5)
                            .build()

                    mJobManager = JobManager(jobConfiguration)
                }

                return mJobManager!!
            }
    }
}
