package id.panislabs.hiplayer.events

import id.panislabs.hiplayer.models.VideoGroup
import id.panislabs.hiplayer.models.VideoInfo

/**
 * @author      paniclabs.
 * @created     on 2017/1/14.
 * @email       panic.inc.dev@gmail.com
 */
class VideoEvents {
    class OnLocalVideoListGot(val videoGroupList: MutableList<VideoGroup>)

    class OpenVideoGroup(val videoGroup: VideoGroup)

    class PlayLocalVideo(val videoInfo: VideoInfo)
}
