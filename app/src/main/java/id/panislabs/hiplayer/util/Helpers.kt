package id.panislabs.hiplayer.util

import android.content.*
import android.database.Cursor
import android.media.AudioManager
import android.preference.PreferenceManager
import android.provider.MediaStore
import android.util.TypedValue
import android.view.View
import android.view.Window
import android.view.WindowManager
import java.io.File
import java.text.SimpleDateFormat
import java.util.*

/**
 * @author      paniclabs.
 * @created     on 2017/1/14.
 * @email       panic.inc.dev@gmail.com
 */
class Helpers(base: Context) : ContextWrapper(base) {

    fun getDensityPixels(pixels: Int): Float {
        return TypedValue.applyDimension(
                TypedValue.COMPLEX_UNIT_DIP, pixels.toFloat(), resources.displayMetrics)
    }

    fun isVideoPortrait(height: Double, width: Double): Boolean {
        return height > width
    }

    val windowManager: WindowManager
        get() = getSystemService(Context.WINDOW_SERVICE) as WindowManager

    fun destroyVideoSurface(mWindowManager: WindowManager?, view: View) {
        mWindowManager?.removeView(view)
    }

    val allVideosUri: ArrayList<String>
        get() {
            val uris = ArrayList<String>()
            val cursor = videosCursor
            val pathColumn = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA)

            while (cursor.moveToNext()) {
                uris.add(cursor.getString(pathColumn))
            }
            cursor.close()
            return uris
        }

    private val videosCursor: Cursor
        get() {
            val Projection = arrayOf(MediaStore.Video.Media._ID, MediaStore.Images.Media.DATA)
            return contentResolver.query(MediaStore.Video.Media.EXTERNAL_CONTENT_URI,
                    Projection, null, null, null)
        }

    fun getVideoTitles(videos: ArrayList<String>): Array<String> {
        val vids = ArrayList<String>()
        for (video in videos) {
            val file = File(video)
            vids.add(file.name)
        }
        val realVideos = arrayOfNulls<String>(vids.size)
        return vids.toTypedArray()
    }

    fun playVideoForLocation(filename: String, startPosition: Int) {
//        val intent = Intent(applicationContext, VideoPlayer::class.java)
//        intent.putExtra("videoUri", filename)
//        intent.putExtra("startPosition", startPosition)
//        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
//        startActivity(intent)
    }

    fun deleteFile(databaseIndex: Int) {
        val projection = arrayOf(MediaStore.Video.Media._ID)
        val cursor = contentResolver.query(MediaStore.Video.Media.EXTERNAL_CONTENT_URI,
                projection, null, null, null)
        val idColumn = cursor!!.getColumnIndexOrThrow(MediaStore.Images.Media._ID)
        cursor.moveToPosition(databaseIndex)
        val id = cursor.getInt(idColumn)
        val uri = ContentUris.withAppendedId(MediaStore.Video.Media.EXTERNAL_CONTENT_URI, id.toLong())
        contentResolver.delete(uri, null, null)
        cursor.close()
    }

    fun getDurationForVideo(databaseIndex: Int): Int {
        val projection = arrayOf(MediaStore.Video.Media.DURATION)
        val cursor = contentResolver.query(MediaStore.Video.Media.EXTERNAL_CONTENT_URI,
                projection, null, null, null)
        val durationColumn = cursor!!.getColumnIndexOrThrow(MediaStore.Video.VideoColumns.DURATION)
        cursor.moveToPosition(databaseIndex)
        val duration = cursor.getString(durationColumn)
        cursor.close()
        return Integer.valueOf(duration)!!
    }

    fun getResolutionForVideo(databaseIndex: Int): String {
        val projection = arrayOf(MediaStore.Video.Media.RESOLUTION)
        val cursor = contentResolver.query(MediaStore.Video.Media.EXTERNAL_CONTENT_URI,
                projection, null, null, null)
        val resolutionColumn = cursor!!.getColumnIndexOrThrow(MediaStore.Video.VideoColumns.RESOLUTION)
        cursor.moveToPosition(databaseIndex)
        val resolution = cursor.getString(resolutionColumn)
        cursor.close()
        return resolution
    }

    fun getCreationDate(databaseIndex: Int): String {
        val projection = arrayOf(MediaStore.Video.Media.DATE_TAKEN)
        val cursor = contentResolver.query(MediaStore.Video.Media.EXTERNAL_CONTENT_URI,
                projection, null, null, null)
        val durationColumn = cursor!!.getColumnIndexOrThrow(MediaStore.Video.VideoColumns.DATE_TAKEN)
        cursor.moveToPosition(databaseIndex)
        val dateTaken = cursor.getString(durationColumn)
        val CreationDate = Helpers.Companion.getDate(dateTaken, "dd-MM-yyy")
        cursor.close()
        return CreationDate
    }

    fun getVideoAlbumName(databaseIndex: Int): String {
        val projection = arrayOf(MediaStore.Video.Media.ALBUM)
        val cursor = contentResolver.query(MediaStore.Video.Media.EXTERNAL_CONTENT_URI,
                projection, null, null, null)
        val albumColumn = cursor!!.getColumnIndexOrThrow(MediaStore.Video.VideoColumns.ALBUM)
        cursor.moveToPosition(databaseIndex)
        val dateTaken = cursor.getString(albumColumn)
        cursor.close()
        return dateTaken
    }

    fun getArtist(databaseIndex: Int): String {
        val projection = arrayOf(MediaStore.Video.Media.ARTIST)
        val cursor = contentResolver.query(MediaStore.Video.Media.EXTERNAL_CONTENT_URI,
                projection, null, null, null)
        val artistColumn = cursor!!.getColumnIndexOrThrow(MediaStore.Video.VideoColumns.ARTIST)
        cursor.moveToPosition(databaseIndex)
        val dateTaken = cursor.getString(artistColumn)
        cursor.close()
        return dateTaken
    }

    fun getVideoTitle(databaseIndex: Int): String {
        val projection = arrayOf(MediaStore.Video.Media.TITLE)
        val cursor = contentResolver.query(MediaStore.Video.Media.EXTERNAL_CONTENT_URI,
                projection, null, null, null)
        val titleColumn = cursor!!.getColumnIndexOrThrow(MediaStore.Video.VideoColumns.TITLE)
        cursor.moveToPosition(databaseIndex)
        val title = cursor.getString(titleColumn)
        cursor.close()
        return title
    }

    fun getLocation(databaseIndex: Int): String {
        val projection = arrayOf(MediaStore.Video.Media.DATA)
        val cursor = contentResolver.query(MediaStore.Video.Media.EXTERNAL_CONTENT_URI,
                projection, null, null, null)
        val dataColumn = cursor!!.getColumnIndexOrThrow(MediaStore.Video.VideoColumns.DATA)
        cursor.moveToPosition(databaseIndex)
        val dateTaken = cursor.getString(dataColumn)
        cursor.close()
        return dateTaken
    }

    fun getVideoCategory(databaseIndex: Int): String {
        val projection = arrayOf(MediaStore.Video.Media.CATEGORY)
        val cursor = contentResolver.query(MediaStore.Video.Media.EXTERNAL_CONTENT_URI,
                projection, null, null, null)
        val categoryColumn = cursor!!.getColumnIndexOrThrow(MediaStore.Video.VideoColumns.CATEGORY)
        cursor.moveToPosition(databaseIndex)
        val dateTaken = cursor.getString(categoryColumn)
        cursor.close()
        return dateTaken
    }

    fun getFormattedTime(timeMs: Int): String {
        val totalSeconds = timeMs / 1000
        val seconds = totalSeconds % 60
        val minutes = totalSeconds / 60 % 60
        val hours = totalSeconds / 3600

        val mFormatBuilder = StringBuilder()
        val mFormatter = Formatter(mFormatBuilder, Locale.getDefault())

        mFormatBuilder.setLength(0)
        if (hours > 0) {
            return mFormatter.format("%d:%02d:%02d", hours, minutes, seconds).toString()
        } else {
            return mFormatter.format("%02d:%02d", minutes, seconds).toString()
        }
    }

    fun setScreenBrightness(window: Window, value: Float) {
        val layoutParams = window.attributes
        layoutParams.screenBrightness = value
        window.attributes = layoutParams
    }

    fun showLauncherHome() {
        val startMain = Intent(Intent.ACTION_MAIN)
        startMain.addCategory(Intent.CATEGORY_HOME)
        startMain.flags = Intent.FLAG_ACTIVITY_NEW_TASK
        startActivity(startMain)
    }

    fun getCurrentBrightness(window: Window): Float {
        return window.attributes.screenBrightness
    }

    val currentVolume: Int
        get() {
            val am = getSystemService(Context.AUDIO_SERVICE) as AudioManager
            return am.getStreamVolume(AudioManager.STREAM_MUSIC)
        }

    fun setVolume(level: Int) {
        val am = getSystemService(Context.AUDIO_SERVICE) as AudioManager
        am.setStreamVolume(AudioManager.STREAM_MUSIC, level, 0)
    }

    var isRepeatEnabled: Boolean
        get() {
            val sharedPreferences = preferenceManager
            return sharedPreferences.getBoolean("repeat_enabled", false)
        }
        set(enable) {
            val sharedPreferences = preferenceManager
            sharedPreferences.edit().putBoolean("repeat_enabled", enable).apply()
        }

    var isShuffleEnabled: Boolean
        get() {
            val sharedPreferences = preferenceManager
            return sharedPreferences.getBoolean("shuffle_enable", false)
        }
        set(enable) {
            val sharedPreferences = preferenceManager
            sharedPreferences.edit().putBoolean("shuffle_enable", enable).apply()
        }

    private val preferenceManager: SharedPreferences
        get() = PreferenceManager.getDefaultSharedPreferences(applicationContext)

    companion object {

        fun getDate(milliSeconds: String, dateFormat: String): String {
            // Create a DateFormatter object for displaying date in specified format.
            val formatter = SimpleDateFormat(dateFormat)

            // Create a calendar object that will convert the date and time value in milliseconds to date.
            val calendar = Calendar.getInstance()
            calendar.timeInMillis = java.lang.Long.parseLong(milliSeconds)
            return formatter.format(calendar.time)
        }
    }
}
