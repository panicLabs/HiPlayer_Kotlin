package id.panislabs.hiplayer.util

import java.text.SimpleDateFormat
import java.util.*
import java.util.regex.Pattern

/**
 * @author      paniclabs.
 * @created     on 2017/1/14.
 * @email       panic.inc.dev@gmail.com
 */
object StringUtils {

    fun notNullNorEmpty(str: String?): Boolean {
        return str != null && !str.isEmpty()
    }

    fun nullOrEmpty(str: String?): Boolean {
        return str == null || str.isEmpty()
    }

    fun join(list: List<String>?, separator: String): String {
        if (list == null || list.size == 0) {
            return ""
        }

        val stringBuilder = StringBuilder()

        for (i in list.indices) {
            stringBuilder.append(list[i])

            if (i < list.size - 1) {
                stringBuilder.append(separator)
            }
        }

        return stringBuilder.toString()
    }

    fun join(array: ArrayList<String>?, separator: String): String {
        val result = StringBuffer()

        if (array != null && array.size > 0) {
            for (str in array) {
                result.append(str)
                result.append(separator)
            }

            result.delete(result.length - 1, result.length)
        }

        return result.toString()
    }

    fun join(array: Array<String>?, separator: String): String {
        if (array == null || array.size == 0) {
            return ""
        }

        val stringBuilder = StringBuilder()

        for (i in array.indices) {
            stringBuilder.append(array[i])

            if (i < array.size - 1) {
                stringBuilder.append(separator)
            }
        }

        return stringBuilder.toString()
    }

    fun getTimeDisplayString(milliSeconds: Long): String {
        val timeFormat = if (milliSeconds > 60 * 60 * 1000) "HH:mm:ss" else "mm:ss"
        val simpleDateFormat = SimpleDateFormat(timeFormat, Locale.ENGLISH)
        simpleDateFormat.timeZone = TimeZone.getTimeZone("GMT+00:00")
        return simpleDateFormat.format(milliSeconds)
    }

    fun isChineseChar(str: String): Boolean {
        var temp = false
        val p = Pattern.compile("[\u4e00-\u9fa5]")
        val m = p.matcher(str)

        if (m.find()) {
            temp = true
        }

        return temp
    }
}
