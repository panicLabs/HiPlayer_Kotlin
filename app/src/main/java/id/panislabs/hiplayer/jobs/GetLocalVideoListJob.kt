package id.panislabs.hiplayer.jobs

import android.provider.MediaStore
import com.birbit.android.jobqueue.Job
import com.birbit.android.jobqueue.Params
import com.birbit.android.jobqueue.RetryConstraint
import id.panislabs.hiplayer.MyApplication
import id.panislabs.hiplayer.events.VideoEvents
import id.panislabs.hiplayer.models.VideoGroup
import id.panislabs.hiplayer.models.VideoInfo
import org.greenrobot.eventbus.EventBus
import java.util.*

/**
 * @author      paniclabs.
 * @created     on 2017/1/14.
 * @email       panic.inc.dev@gmail.com
 */
class GetLocalVideoListJob : Job(Params(JobPriority.HIGH).requireNetwork()) {

    override fun onAdded() {

    }

    @Throws(Throwable::class)
    override fun onRun() {
        val videoGroupHashMap = HashMap<String, VideoGroup>()

        val thumbColumns = arrayOf(MediaStore.Video.Thumbnails.DATA, MediaStore.Video.Thumbnails.VIDEO_ID)

        val mediaColumns = arrayOf(MediaStore.Video.Media._ID, MediaStore.Video.Media.DATA, MediaStore.Video.Media.TITLE, MediaStore.Video.Media.MIME_TYPE, MediaStore.Video.Media.DISPLAY_NAME, MediaStore.Video.Media.DURATION, MediaStore.Video.Media.SIZE)

        val cursor = MyApplication.instance?.getContentResolver()?.query(
                MediaStore.Video.Media.EXTERNAL_CONTENT_URI, mediaColumns, null, null, null)

        if (cursor != null && cursor!!.moveToFirst()) {
            do {
                val videoInfo = VideoInfo()

                val mediaId = cursor!!.getInt(cursor!!.getColumnIndex(MediaStore.Video.Media._ID))

                val thumbCursor = MyApplication.instance?.getContentResolver()?.query(
                        MediaStore.Video.Thumbnails.EXTERNAL_CONTENT_URI, thumbColumns,
                        MediaStore.Video.Thumbnails.VIDEO_ID + "=" + mediaId,
                        null,
                        null)

                if (thumbCursor != null && thumbCursor!!.moveToFirst()) {
                    videoInfo.thumbPath = thumbCursor!!.getString(thumbCursor!!.getColumnIndex(MediaStore.Video.Thumbnails.DATA))
                }

                videoInfo.title = cursor!!.getString(cursor!!.getColumnIndexOrThrow(MediaStore.Video.Media.TITLE))
                videoInfo.displayName = cursor!!.getString(cursor!!.getColumnIndexOrThrow(MediaStore.Video.Media.DISPLAY_NAME))

                val duration = cursor!!.getInt(cursor!!.getColumnIndexOrThrow(MediaStore.Video.Media.DURATION))
                videoInfo.duration = duration

                videoInfo.size = cursor!!.getInt(cursor!!.getColumnIndexOrThrow(MediaStore.Video.Media.SIZE)).toLong()

                val videoPath = cursor!!.getString(cursor!!.getColumnIndexOrThrow(MediaStore.Video.Media.DATA))
                videoInfo.path = videoPath

                val videoFolderPath = videoPath.substring(0, videoPath.lastIndexOf("/"))
                videoInfo.folderPath = videoFolderPath

                val videoGroupName = videoFolderPath.substring(videoFolderPath.lastIndexOf("/") + 1)

                if (!videoGroupHashMap.containsKey(videoGroupName)) {
                    val videoGroup = VideoGroup()
                    videoGroup.groupName = videoGroupName

                    val videoInfoList = ArrayList<VideoInfo>()
                    videoInfoList.add(videoInfo)
                    videoGroup.videoInfoList = videoInfoList

                    videoGroupHashMap.put(videoGroupName, videoGroup)
                } else {
                    videoGroupHashMap[videoGroupName]!!.videoInfoList?.add(videoInfo)
                }
            } while (cursor!!.moveToNext())
        }

        EventBus.getDefault().post(VideoEvents.OnLocalVideoListGot(ArrayList(videoGroupHashMap.values)))
    }

    override fun onCancel(cancelReason: Int, throwable: Throwable?) {

    }

    override fun shouldReRunOnThrowable(throwable: Throwable, runCount: Int, maxRunCount: Int): RetryConstraint? {
        return null
    }
}
