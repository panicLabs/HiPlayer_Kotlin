package id.panislabs.hiplayer.jobs

/**
 * @author      paniclabs.
 * @created     on 2017/1/14.
 * @email       panic.inc.dev@gmail.com
 */
object JobPriority {
    var LOW = 0
    var MID = 500
    var HIGH = 1000
}
