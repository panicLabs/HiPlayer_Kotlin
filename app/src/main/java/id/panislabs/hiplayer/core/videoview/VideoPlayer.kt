//package com.shirlman.yiplayer.core.videoview
//
//import android.app.Activity
//import android.content.Context
//import android.content.Intent
//import android.content.IntentFilter
//import android.content.pm.ActivityInfo
//import android.media.MediaPlayer
//import android.os.Bundle
//import android.support.v4.view.GestureDetectorCompat
//import android.view.GestureDetector
//import android.view.MotionEvent
//import android.view.View
//import android.widget.Button
//import android.widget.FrameLayout
//import android.widget.MediaController
//
//import com.shirlman.yiplayer.R
//import id.panislabs.hiplayer.Helpers
//
//import java.util.Random
//
//
//class VideoPlayer : Activity(), MediaPlayer.OnCompletionListener, View.OnClickListener, CustomVideoView.MediaPlayerStateChangedListener {
//
//    private var mCustomVideoView: CustomVideoView? = null
//    private var isLandscape = true
//    private var mHelpers: Helpers? = null
//    private var mButtonsFrameTop: FrameLayout? = null
//    private var mDetector: GestureDetectorCompat? = null
//    private var mScreenStateListener: ScreenStateListener? = null
//
//    private class Screen {
//        internal object Brightness {
//            val HIGH = 1f
//            val LOW = 0f
//        }
//    }
//
//    private class Sound {
//        internal object Level {
//            val MINIMUM = 0
//            val MAXIMUM = 15
//        }
//    }
//
//    override fun onCreate(savedInstanceState: Bundle?) {
//        super.onCreate(savedInstanceState)
//        setContentView(R.layout.activity_video_player)
//        mHelpers = Helpers(applicationContext)
//        mScreenStateListener = ScreenStateListener(mCustomVideoView)
//        val filter = IntentFilter(Intent.ACTION_SCREEN_OFF)
//        mDetector = GestureDetectorCompat(this, GestureListener())
//        mButtonsFrameTop = findViewById(R.id.buttons_frame_top) as FrameLayout
//        val overlayButton = findViewById(R.id.overlayButton) as Button
//        val rotationButton = findViewById(R.id.bRotate) as Button
//        overlayButton.setOnClickListener(this)
//        rotationButton.setOnClickListener(this)
//        val bundle = intent.extras
//        val videoPath = bundle.getString("videoUri")
//        val seekPosition = bundle.getInt("startPosition")
//        mCustomVideoView = findViewById(R.id.videoSurface) as CustomVideoView
//        mCustomVideoView!!.setMediaPlayerStateChangedListener(this)
//        mCustomVideoView!!.setOnCompletionListener(this)
//        mHelpers!!.setScreenBrightness(window, Screen.Brightness.HIGH)
//        val mediaController = CustomMediaController(this)
//        mediaController.setAnchorView(mCustomVideoView)
//        mCustomVideoView!!.setMediaController(mediaController)
//        registerReceiver(mScreenStateListener, filter)
//        mCustomVideoView!!.setVideoPath(videoPath)
//        mCustomVideoView!!.seekTo(seekPosition)
//        mCustomVideoView!!.start()
//    }
//
//    override fun onPause() {
//        super.onPause()
//        mCustomVideoView!!.pause()
//        try {
//            unregisterReceiver(mScreenStateListener)
//        } catch (ignored: IllegalArgumentException) {
//        }
//
//    }
//
//    override fun onTouchEvent(event: MotionEvent): Boolean {
//        this.mDetector!!.onTouchEvent(event)
//        return super.onTouchEvent(event)
//    }
//
//    override fun onClick(v: View) {
//        when (v.id) {
//            R.id.overlayButton -> {
//                val videoOverlay = VideoOverlay(applicationContext)
//                videoOverlay.setVideoFile(mCustomVideoView!!.videoURI)
//                videoOverlay.setVideoStartPosition(mCustomVideoView!!.currentPosition)
//                videoOverlay.setVideoHeight(mCustomVideoView!!.videoHeight)
//                videoOverlay.setVideoWidth(mCustomVideoView!!.videoWidth)
//                videoOverlay.setPlayOnStart(mCustomVideoView!!.isPlaying)
//                mCustomVideoView!!.pause()
//                videoOverlay.startPlayback()
//                mCustomVideoView!!.stopPlayback()
//                finish()
//                mHelpers!!.showLauncherHome()
//            }
//            R.id.bRotate -> {
//                if (isLandscape) {
//                    requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
//                } else {
//                    requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE
//                }
//                isLandscape = !isLandscape
//            }
//        }
//    }
//
//    override fun onCompletion(mp: MediaPlayer) {
//        if (mHelpers!!.isRepeatEnabled) {
//            mp.start()
//        } else if (mHelpers!!.isShuffleEnabled) {
//            val randomizer = Random()
//            mHelpers!!.playVideoForLocation(MainActivity.mVideosPathList.get(
//                    randomizer.nextInt(MainActivity.mVideosPathList.size())), 0)
//        } else {
//            finish()
//        }
//    }
//
//    override fun onPlaybackStateChanged(state: Int) {
//
//    }
//
//    override fun onVideoViewPrepared(mp: MediaPlayer) {
//        setVideoOrientation()
//    }
//
//    internal inner class CustomMediaController(context: Context) : MediaController(context) {
//
//        override fun show() {
//            super.show()
//            mButtonsFrameTop!!.visibility = View.VISIBLE
//        }
//
//        override fun hide() {
//            super.hide()
//            mButtonsFrameTop!!.visibility = View.INVISIBLE
//        }
//    }
//
//    internal inner class GestureListener : GestureDetector.SimpleOnGestureListener() {
//
//        val BRIGHTNESS_STEP = 0.066
//        val VOLUME_STEP = 1
//        private var lastTrackedPosition: Float = 0.toFloat()
//
//        override fun onDown(e: MotionEvent): Boolean {
//            lastTrackedPosition = e.y
//            return super.onDown(e)
//        }
//
//        override fun onScroll(e1: MotionEvent, e2: MotionEvent, distanceX: Float, distanceY: Float): Boolean {
//            val ACTIVITY_HEIGHT_FRAGMENT = activityHeight / 50
//            val touchX = e2.x
//            val touchY = e2.y
//            if (touchX < activityWidth / 2) {
//                var brightness = mHelpers!!.getCurrentBrightness(window)
//                if (touchY >= lastTrackedPosition + ACTIVITY_HEIGHT_FRAGMENT && brightness - BRIGHTNESS_STEP > Screen.Brightness.LOW) {
//                    brightness -= BRIGHTNESS_STEP.toFloat()
//                    mHelpers!!.setScreenBrightness(window, brightness)
//                    lastTrackedPosition = touchY
//                } else if (touchY <= lastTrackedPosition - ACTIVITY_HEIGHT_FRAGMENT && brightness + BRIGHTNESS_STEP <= Screen.Brightness.HIGH) {
//                    brightness += BRIGHTNESS_STEP.toFloat()
//                    mHelpers!!.setScreenBrightness(window, brightness)
//                    lastTrackedPosition = touchY
//                }
//            } else {
//                var currentVolume = mHelpers!!.currentVolume
//                if (touchY > lastTrackedPosition + ACTIVITY_HEIGHT_FRAGMENT && currentVolume - VOLUME_STEP >= Sound.Level.MINIMUM) {
//                    currentVolume -= VOLUME_STEP
//                    mHelpers!!.setVolume(currentVolume)
//                    lastTrackedPosition = touchY
//                } else if (touchY <= lastTrackedPosition - ACTIVITY_HEIGHT_FRAGMENT && currentVolume + VOLUME_STEP <= Sound.Level.MAXIMUM) {
//                    currentVolume += VOLUME_STEP
//                    mHelpers!!.setVolume(currentVolume)
//                    lastTrackedPosition = touchY
//                }
//            }
//            return super.onScroll(e1, e2, distanceX, distanceY)
//        }
//    }
//
//    private val activityHeight: Int
//        get() = window.decorView.height
//
//    private val activityWidth: Int
//        get() = window.decorView.width
//
//    private fun setVideoOrientation() {
//        if (mHelpers!!.isVideoPortrait(mCustomVideoView!!.videoHeight.toDouble(),
//                mCustomVideoView!!.videoWidth.toDouble())) {
//            requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
//        } else {
//            requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE
//        }
//    }
//}
