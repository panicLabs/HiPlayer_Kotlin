//package com.shirlman.yiplayer.core.videoview
//
//import android.content.Context
//import android.content.Intent
//import android.content.IntentFilter
//import android.graphics.PixelFormat
//import android.media.MediaPlayer
//import android.net.Uri
//import android.support.v4.view.GestureDetectorCompat
//import android.view.GestureDetector
//import android.view.Gravity
//import android.view.MotionEvent
//import android.view.SurfaceHolder
//import android.view.View
//import android.view.WindowManager
//import android.widget.Button
//import android.widget.LinearLayout
//import android.widget.RelativeLayout
//import com.shirlman.yiplayer.R
//import id.panislabs.hiplayer.Helpers
//
//class VideoOverlay(private val mContext: Context) : RelativeLayout(mContext), SurfaceHolder.Callback, MediaPlayer.OnCompletionListener, View.OnClickListener, CustomVideoView.MediaPlayerStateChangedListener {
//
//    private val filter = IntentFilter(Intent.ACTION_SCREEN_OFF)
//    private var mWindowManager: WindowManager? = null
//    private var mFileRepo: Uri? = null
//    private var mPosition: Int = 0
//    private var params: WindowManager.LayoutParams? = null
//    private val mClose: Button
//    private val mScreenStateListener: ScreenStateListener
//    private var mVideoHeight: Double = 0.toDouble()
//    private var mVideoWidth: Double = 0.toDouble()
//    private var mPlayOnStart: Boolean = false
//    private val mHelpers: Helpers
//    private val mDetector: GestureDetectorCompat
//    private val mCustomVideoView: CustomVideoView
//
//    init {
//        mHelpers = Helpers(mContext)
//        mCustomVideoView = CustomVideoView(mContext)
//        mClose = closeButton
//        addView(mCustomVideoView)
//        addView(mClose)
//        mCustomVideoView.setOnCompletionListener(this)
//        mCustomVideoView.setMediaPlayerStateChangedListener(this)
//        mScreenStateListener = ScreenStateListener(mCustomVideoView)
//        val holder = mCustomVideoView.holder
//        holder.addCallback(this)
//        mDetector = GestureDetectorCompat(mContext, GestureListener())
//    }
//
//    internal fun setVideoFile(uri: Uri) {
//        mFileRepo = uri
//    }
//
//    internal fun setVideoStartPosition(position: Int) {
//        this.mPosition = position
//    }
//
//    internal fun setVideoHeight(height: Int) {
//        mVideoHeight = height.toDouble()
//    }
//
//    internal fun setVideoWidth(width: Int) {
//        mVideoWidth = width.toDouble()
//    }
//
//    internal fun setPlayOnStart(start: Boolean) {
//        mPlayOnStart = start
//    }
//
//    internal fun startPlayback() {
//        createSystemOverlayForPreview(this)
//    }
//
//    override fun surfaceCreated(holder: SurfaceHolder) {
//        mContext.registerReceiver(mScreenStateListener, filter)
//        mCustomVideoView.setVideoURI(mFileRepo!!)
//        mCustomVideoView.seekTo(mPosition)
//        if (mPlayOnStart) {
//            mCustomVideoView.start()
//        }
//    }
//
//    override fun surfaceChanged(holder: SurfaceHolder, format: Int, width: Int, height: Int) {
//
//    }
//
//    override fun surfaceDestroyed(holder: SurfaceHolder) {
//        mContext.unregisterReceiver(mScreenStateListener)
//
//    }
//
//    private fun createSystemOverlayForPreview(previewForCamera: View) {
//        mWindowManager = mHelpers.getWindowManager()
//        params = customWindowManagerParameters
//        mWindowManager!!.addView(previewForCamera, params)
//    }
//
//    private val customWindowManagerParameters: WindowManager.LayoutParams
//        get() {
//            val height: Double
//            val width: Double
//            val ratio: Double
//            if (mHelpers.isVideoPortrait(mVideoHeight, mVideoWidth)) {
//                width = mHelpers.getDensityPixels(150).toDouble()
//                ratio = mVideoHeight / mVideoWidth
//                height = width * ratio
//            } else {
//                height = mHelpers.getDensityPixels(150)
//                ratio = mVideoWidth / mVideoHeight
//                width = height * ratio
//            }
//            val params = WindowManager.LayoutParams()
//            params.height = height.toInt()
//            params.width = width.toInt()
//            params.type = WindowManager.LayoutParams.TYPE_PHONE
//            params.flags = WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE or WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS
//            params.format = PixelFormat.TRANSLUCENT
//            params.gravity = Gravity.TOP or Gravity.START
//            return params
//        }
//
//    override fun onTouchEvent(event: MotionEvent): Boolean {
//        this.mDetector.onTouchEvent(event)
//        return super.onTouchEvent(event)
//    }
//
//    override fun onCompletion(mp: MediaPlayer) {
//        mHelpers.destroyVideoSurface(mWindowManager, this)
//    }
//
//    override fun onClick(v: View) {
//        when (v.id) {
//            R.id.bClose -> {
//                mCustomVideoView.stopPlayback()
//                mHelpers.destroyVideoSurface(mWindowManager, this)
//            }
//        }
//    }
//
//    override fun onPlaybackStateChanged(state: Int) {
//        when (state) {
//            CustomVideoView.getPLAYING() -> mClose.visibility = View.GONE
//            CustomVideoView.getPAUSED() -> mClose.visibility = View.VISIBLE
//        }
//    }
//
//    override fun onVideoViewPrepared(mp: MediaPlayer) {
//
//    }
//
//    internal inner class GestureListener : GestureDetector.SimpleOnGestureListener() {
//
//        private var initialX = 0
//        private var initialY = 0
//        private var initialTouchX = 0f
//        private var initialTouchY = 0f
//
//        override fun onDown(e: MotionEvent): Boolean {
//            initialX = params!!.x
//            initialY = params!!.y
//            initialTouchX = e.rawX
//            initialTouchY = e.rawY
//            return super.onDown(e)
//        }
//
//        override fun onScroll(e1: MotionEvent, e2: MotionEvent, distanceX: Float, distanceY: Float): Boolean {
//            params!!.x = initialX + (e2.rawX - initialTouchX).toInt()
//            params!!.y = initialY + (e2.rawY - initialTouchY).toInt()
//            mWindowManager!!.updateViewLayout(this@VideoOverlay, params)
//            return super.onScroll(e1, e2, distanceX, distanceY)
//        }
//
//        override fun onSingleTapUp(e: MotionEvent): Boolean {
//            if (mCustomVideoView.isPlaying) {
//                mCustomVideoView.pause()
//            } else {
//                mCustomVideoView.start()
//            }
//            return super.onSingleTapUp(e)
//        }
//
//        override fun onLongPress(e: MotionEvent) {
//            super.onLongPress(e)
//            mCustomVideoView.pause()
//            mHelpers.playVideoForLocation(mFileRepo!!.path,
//                    mCustomVideoView.currentPosition)
//            mHelpers.destroyVideoSurface(mWindowManager, this@VideoOverlay)
//        }
//    }
//
//    private val layoutParametersForCloseButton: LinearLayout.LayoutParams
//        get() {
//            val height = Math.round(mHelpers.getDensityPixels(40))
//            val width = Math.round(mHelpers.getDensityPixels(40))
//            val layoutParams = LinearLayout.LayoutParams(width, height)
//            layoutParams.gravity = Gravity.TOP or Gravity.END
//            return layoutParams
//        }
//
//    private val closeButton: Button
//        get() {
//            val button = Button(mContext)
//            button.text = "X"
//            button.layoutParams = layoutParametersForCloseButton
//            button.setOnClickListener(this)
//            button.id = R.id.bClose
//            button.visibility = View.INVISIBLE
//            return button
//        }
//}
