package id.panislabs.hiplayer.core.base

import android.annotation.SuppressLint
import android.annotation.TargetApi
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log

/**
 * @author      paniclabs.
 * @created     on 2017/1/14.
 * @email       panic.inc.dev@gmail.com
 */
abstract class BaseActivity : AppCompatActivity() {
    protected var permission: Array<String>? = null

    @SuppressLint("NewApi")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        checkPermission()

    }

    abstract fun getPermissionArray()


    @TargetApi(Build.VERSION_CODES.M)
    protected fun checkPermission() {

        if (!hasRequiredPermissions()) {
            requestPermissions(permission!!, 2)//no permission
            return
        } else {
            init()//has permission
        }
    }

    protected fun handlerPermission(requestCode: Int, permissions: Array<String>,
                                    grantResults: IntArray) {


        if (requestCode == 2) {
            if (grantResults.size > 0) {


                for (grantResult in grantResults) {

                    Log.w("PERMISI", "= " + grantResult + " = " + PackageManager.PERMISSION_DENIED)
                    if (grantResult == PackageManager.PERMISSION_DENIED) {


                        this.finish()
                        return

                    } else
                        init()
                }

                Log.w("PERMISI", "= " + grantResults.size)

                init()

            }
        }


    }


    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>,
                                            grantResults: IntArray) {

        handlerPermission(requestCode, permissions, grantResults)


    }


    protected open fun init() {

    }


    private fun hasRequiredPermissions(): Boolean {

        getPermissionArray()
        return hasPermissions(permission!!)
    }

    @SuppressLint("NewApi")
    private fun hasPermissions(permissions: Array<String>): Boolean {
        for (permission in permissions) {
            if (!hasPermission(permission)) {
                return false
            }
        }
        return true
    }

    @TargetApi(Build.VERSION_CODES.M)
    private fun hasPermission(permission: String): Boolean {
        if (checkSelfPermission(permission) != PackageManager.PERMISSION_GRANTED) {
            return false
        }
        return true

    }

    companion object {

        private val PERMISSION_READ_STORAGE = 2
    }


}
