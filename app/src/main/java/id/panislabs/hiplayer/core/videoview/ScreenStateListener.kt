package id.panislabs.hiplayer.core.videoview

/**
 * @author paniclabs.
 * *
 * @created on 1/18/17.
 * *
 * @email panic.inc.dev@gmail.com
 * *
 * @projectName YiPlayer-master
 * *
 * @packageName com.shirlman.yiplayer.core.videoview.
 * *
 * @className ${CLASS}.
 */
class ScreenStateListener
