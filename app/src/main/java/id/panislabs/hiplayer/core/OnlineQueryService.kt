package id.panislabs.hiplayer.core

import id.panislabs.hiplayer.models.ICibaResponse
import id.panislabs.hiplayer.models.ShooterSubtitleDetailResponse
import id.panislabs.hiplayer.models.ShooterSubtitleResponse
import id.panislabs.hiplayer.models.YoudaoResponse
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.QueryMap

/**
 * @author      paniclabs.
 * @created     on 2017/1/14.
 * @email       panic.inc.dev@gmail.com
 */
interface OnlineQueryService {
    @GET("http://fanyi.youdao.com/openapi.do")
    fun queryWordFromYoudao(@QueryMap filters: Map<String, Any>): Call<YoudaoResponse>

    @GET("http://dict-co.iciba.com/api/dictionary.php")
    fun queryWordFromICiba(@QueryMap filters: Map<String, Any>): Call<ICibaResponse>

    @GET("http://api.assrt.net/v1/sub/search")
    fun querySubtitleFromShooter(@QueryMap filters: Map<String, Any>): Call<ShooterSubtitleResponse>

    @GET("http://api.assrt.net/v1/sub/detail")
    fun getSubtitleDetailFromShooter(@QueryMap filters: Map<String, Any>): Call<ShooterSubtitleDetailResponse>
}
