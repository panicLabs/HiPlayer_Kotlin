package id.panislabs.hiplayer.core.videoview

import android.content.Context
import android.media.MediaPlayer
import android.net.Uri
import android.util.AttributeSet
import android.widget.VideoView
import java.util.*

/**
 * @author      paniclabs.
 * @created     on 2017/1/14.
 * @email       panic.inc.dev@gmail.com
 */
class CustomVideoView : VideoView, MediaPlayer.OnPreparedListener {

    private var mCurrentlyPlayingVideoUri: Uri? = null
    private val mListeners = ArrayList<MediaPlayerStateChangedListener>()
    var videoHeight: Int = 0
        private set
    var videoWidth: Int = 0
        private set

    constructor(context: Context) : super(context) {
        setOnPreparedListener(this)
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        setOnPreparedListener(this)
    }

    fun setMediaPlayerStateChangedListener(listener: CustomVideoView.MediaPlayerStateChangedListener) {
        mListeners.add(listener)
    }

    override fun start() {
        super.start()
        keepScreenOn = true
        for (listener in mListeners) {
            listener.onPlaybackStateChanged(1)
        }
    }

    override fun pause() {
        super.pause()
        keepScreenOn = false
        for (listener in mListeners) {
            listener.onPlaybackStateChanged(0)
        }
    }

    override fun stopPlayback() {
        super.stopPlayback()
        mCurrentlyPlayingVideoUri = null
    }

    override fun setVideoURI(uri: Uri) {
        super.setVideoURI(uri)
        mCurrentlyPlayingVideoUri = uri
    }

    fun getVideoURI(): Uri {
        return mCurrentlyPlayingVideoUri!!
    }

    override fun onPrepared(mp: MediaPlayer) {
        videoHeight = mp.videoHeight
        videoWidth = mp.videoWidth
        for (listener in mListeners) {
            listener.onVideoViewPrepared(mp)
        }

    }

    interface MediaPlayerStateChangedListener {
        fun onPlaybackStateChanged(state: Int)
        fun onVideoViewPrepared(mp: MediaPlayer)
    }

    companion object {

        internal val PLAYING = 1
        internal val PAUSED = 0
    }
}
