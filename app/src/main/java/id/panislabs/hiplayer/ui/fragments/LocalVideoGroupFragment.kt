package id.panislabs.hiplayer.ui.fragments

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import id.paniclabs.hiplayer.R
import id.panislabs.hiplayer.MyApplication
import id.panislabs.hiplayer.events.VideoEvents
import id.panislabs.hiplayer.jobs.GetLocalVideoListJob
import id.panislabs.hiplayer.ui.adapters.LocalVideoGroupAdapter
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode

/**
 * @author      paniclabs.
 * @created     on 2017/1/14.
 * @email       panic.inc.dev@gmail.com
 */
class LocalVideoGroupFragment : Fragment() {
    private var mRootView: View? = null
    private var mRecyclerView: RecyclerView? = null

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        super.onCreateView(inflater, container, savedInstanceState)

        EventBus.getDefault().register(this)

        MyApplication.jobManager.addJobInBackground(GetLocalVideoListJob())

        mRootView = inflater!!.inflate(R.layout.local_video_fragment, container, false)

        val layoutManager = LinearLayoutManager(activity)
        mRecyclerView = mRootView!!.findViewById(R.id.local_video_recycler_view) as RecyclerView
        mRecyclerView!!.layoutManager = layoutManager
        mRecyclerView!!.isNestedScrollingEnabled = false

        return mRootView
    }

    override fun onDestroyView() {
        super.onDestroyView()

        EventBus.getDefault().unregister(this)
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onEventMainThread(event: VideoEvents.OnLocalVideoListGot) {
        val videoGroupList = event.videoGroupList

        if (videoGroupList.size > 0) {
            mRecyclerView!!.adapter = LocalVideoGroupAdapter(activity, videoGroupList)
        }
    }
}