package id.panislabs.hiplayer.ui.adapters

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.TextView
import com.nostra13.universalimageloader.core.ImageLoader
import id.paniclabs.hiplayer.R
import id.panislabs.hiplayer.events.VideoEvents
import id.panislabs.hiplayer.models.VideoGroup
import id.panislabs.hiplayer.util.FileUtils
import id.panislabs.hiplayer.util.StringUtils
import org.greenrobot.eventbus.EventBus


/**
 * @author      paniclabs.
 * @created     on 2017/1/14.
 * @email       panic.inc.dev@gmail.com
 */
class LocalVideoAdapter(context: Context, private val mVideoGroup: VideoGroup) : RecyclerView.Adapter<LocalVideoAdapter.ViewHolder>() {
    private val mLayoutInflater: LayoutInflater

    init {
        mLayoutInflater = LayoutInflater.from(context)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val rootView = mLayoutInflater.inflate(R.layout.local_video_item, parent, false)

        return ViewHolder(rootView)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val videoInfo = mVideoGroup.videoInfoList!![position]

        ImageLoader.getInstance().displayImage("file://" + videoInfo.thumbPath, holder.videoThumb)

        holder.videoTitle.text = videoInfo.title
        holder.videoDuration.text = StringUtils.getTimeDisplayString(videoInfo.duration.toLong())
        holder.videoSize.text = FileUtils.showFileSize(videoInfo.size)

        holder.videoThumb.setOnClickListener { EventBus.getDefault().post(VideoEvents.PlayLocalVideo(videoInfo)) }
    }

    override fun getItemCount(): Int {
        return mVideoGroup.videoInfoList!!.size
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var videoThumb: ImageButton
        var videoTitle: TextView
        var videoSize: TextView
        var videoDuration: TextView

        init {

            videoThumb = itemView.findViewById(R.id.local_video_thumb) as ImageButton
            videoTitle = itemView.findViewById(R.id.local_video_title) as TextView
            videoSize = itemView.findViewById(R.id.local_video_info) as TextView
            videoDuration = itemView.findViewById(R.id.local_video_duration) as TextView
        }
    }
}