package id.panislabs.hiplayer.ui.adapters

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import id.paniclabs.hiplayer.R
import id.panislabs.hiplayer.core.OnlineQueryService
import id.panislabs.hiplayer.models.ShooterSubtitleDetailResponse
import id.panislabs.hiplayer.models.ShooterSubtitleResponse
import id.panislabs.hiplayer.util.StringUtils
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.*

/**
* @author      paniclabs.
* @created     on 2017/1/14.
* @email       panic.inc.dev@gmail.com
*/
class OnlineSubtitleAdapter(context: Context, private val mSubtitleList: List<ShooterSubtitleResponse.SubBean.SubsBean>) : RecyclerView.Adapter<OnlineSubtitleAdapter.ViewHolder>() {
    private val mLayoutInflater: LayoutInflater

    private inner class OnDownloadClickListener internal constructor(private val mSubtitleId: String) : View.OnClickListener {

        override fun onClick(view: View) {
            val shooterBaseUrl = "http://api.assrt.net/v1/sub/detail/"
            val shooterToken = "5fjG5Znw0KgfqL1QmDffB3A7qzaGAXzF"

            val searchFilters = LinkedHashMap<String, Any>()
            searchFilters.put("token", shooterToken)
            searchFilters.put("id", mSubtitleId)

            val retrofit = Retrofit.Builder()
                    .baseUrl(shooterBaseUrl)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build()

            val service = retrofit.create(OnlineQueryService::class.java)
            service.getSubtitleDetailFromShooter(searchFilters).enqueue(object : Callback<ShooterSubtitleDetailResponse> {
                override fun onResponse(call: Call<ShooterSubtitleDetailResponse>, response: Response<ShooterSubtitleDetailResponse>) {
                    if (response.isSuccessful && response.body() != null && response.body().sub != null
                            && response.body().sub!!.subs != null && response.body().sub!!.subs!!.size > 0) {
                        val downloadUrl = response.body().sub!!.subs!![0].url
                    }
                }

                override fun onFailure(call: Call<ShooterSubtitleDetailResponse>, t: Throwable) {
                    Log.e("OnDownloadClickListener", "querySubtitleFromShooter.onFailure: ", t)
                }
            })
        }
    }

    init {
        mLayoutInflater = LayoutInflater.from(context)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val rootView = mLayoutInflater.inflate(R.layout.online_subtitle_item, parent, false)

        return ViewHolder(rootView)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val subtitle = mSubtitleList[position]

        val title = if (StringUtils.nullOrEmpty(subtitle.native_name))
            subtitle.videoname
        else
            subtitle.native_name
        val language = if (subtitle.lang == null) "tidak diketahui" else subtitle.lang?.desc

        holder.title.text = title
        holder.voteScore.text = subtitle.vote_score
        holder.language.text = language
        holder.download.setOnClickListener(OnDownloadClickListener(subtitle.id!!))
    }

    override fun getItemCount(): Int {
        return mSubtitleList.size
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var title: TextView
        var language: TextView
        var voteScore: TextView
        val download: Button

        init {

            title = itemView.findViewById(R.id.online_subtitle_title) as TextView
            language = itemView.findViewById(R.id.online_subtitle_language) as TextView
            voteScore = itemView.findViewById(R.id.online_subtitle_vote_score) as TextView
            download = itemView.findViewById(R.id.online_subtitle_download) as Button
        }
    }
}