package id.panislabs.hiplayer.ui.fragments

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import id.paniclabs.hiplayer.R
import id.panislabs.hiplayer.models.VideoGroup
import id.panislabs.hiplayer.ui.adapters.LocalVideoAdapter

/**
 * @author      paniclabs.
 * @created     on 2017/1/14.
 * @email       panic.inc.dev@gmail.com
 */
class LocalVideoFragment : Fragment() {
    private var mRootView: View? = null
    private var mRecyclerView: RecyclerView? = null

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        super.onCreateView(inflater, container, savedInstanceState)

        val videoGroup = arguments.getSerializable(VideoGroup::class.java.simpleName) as VideoGroup

        mRootView = inflater!!.inflate(R.layout.local_video_fragment, container, false)

        val gridLayoutManager = GridLayoutManager(activity, 2)
        mRecyclerView = mRootView!!.findViewById(R.id.local_video_recycler_view) as RecyclerView
        mRecyclerView!!.layoutManager = gridLayoutManager
        mRecyclerView!!.isNestedScrollingEnabled = false

        mRecyclerView!!.adapter = LocalVideoAdapter(activity, videoGroup)

        return mRootView
    }
}