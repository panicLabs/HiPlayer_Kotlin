package id.panislabs.hiplayer.ui.adapters

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import id.paniclabs.hiplayer.R
import id.panislabs.hiplayer.events.VideoEvents
import id.panislabs.hiplayer.models.VideoGroup
import org.greenrobot.eventbus.EventBus

/**
 * @author      paniclabs.
 * @created     on 2017/1/14.
 * @email       panic.inc.dev@gmail.com
 */
class LocalVideoGroupAdapter
    (context: Context, private val mVideoGroupList: List<VideoGroup>) : RecyclerView.Adapter<LocalVideoGroupAdapter.ViewHolder>() {
    private val mLayoutInflater: LayoutInflater

    init {
        mLayoutInflater = LayoutInflater.from(context)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val rootView = mLayoutInflater.inflate(R.layout.local_video_grup_item, parent, false)

        return ViewHolder(rootView)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val videoGroup = mVideoGroupList[position]
        holder.folderName?.text = videoGroup.groupName

        val videoCount = String.format(
                holder.itemView.resources.getString(R.string.local_video_count),
                videoGroup.videoInfoList!!.size)
        holder.videoCount?.text = videoCount

        holder.itemView.setOnClickListener { EventBus.getDefault().post(VideoEvents.OpenVideoGroup(videoGroup)) }
    }

    override fun getItemCount(): Int {
        return mVideoGroupList.size
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var folderName: TextView
        var videoCount: TextView

        init {
            folderName = itemView.findViewById(R.id.name_folder) as TextView
            videoCount = itemView.findViewById(R.id.total_video) as TextView
        }
    }
}