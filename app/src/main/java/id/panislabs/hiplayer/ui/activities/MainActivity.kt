package id.panislabs.hiplayer.ui.activities

import android.Manifest
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentTransaction
import android.support.v7.widget.Toolbar
import android.view.Menu
import android.view.MenuItem
import fm.jiecao.jcvideoplayer_lib.JCVideoPlayer
import id.paniclabs.hiplayer.R
import id.panislabs.hiplayer.core.base.BaseActivity
import id.panislabs.hiplayer.events.VideoEvents
import id.panislabs.hiplayer.models.VideoGroup
import id.panislabs.hiplayer.models.VideoInfo
import id.panislabs.hiplayer.ui.fragments.LocalVideoFragment
import id.panislabs.hiplayer.ui.fragments.LocalVideoGroupFragment
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode



/**
 * @author      paniclabs.
 * @created     on 2017/1/14.
 * @email       panic.inc.dev@gmail.com
 */
class MainActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        EventBus.getDefault().register(this)
        setContentView(R.layout.app_bar_main)

        val toolbar = findViewById(R.id.toolbar) as Toolbar
        setSupportActionBar(toolbar)
        supportActionBar?.title = ""

        supportFragmentManager.beginTransaction().add(R.id.fragment_container, LocalVideoGroupFragment()).commit()
    }

    override fun getPermissionArray() {

        if (permission == null) {

            permission = arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE)


        }

    }

    public override fun init() {

    }

    override fun onBackPressed() {
        if(JCVideoPlayer.backPress()) {
            return
        }
        super.onBackPressed()
    }

    override fun onPause() {
        super.onPause()
        JCVideoPlayer.releaseAllVideos()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        val id = item.itemId


        if (id == R.id.action_settings) {
            return true
        }

        return super.onOptionsItemSelected(item)
    }


    override fun onDestroy() {
        super.onDestroy()

        EventBus.getDefault().unregister(this)
    }

    private val currentFragment: Fragment
        get() {
            var currentFragment: Fragment? = null

            val fragmentList = supportFragmentManager.fragments

            if (fragmentList != null) {
                for (fragment in fragmentList) {
                    if (fragment != null && fragment.isVisible) {
                        currentFragment = fragment

                        break
                    }
                }
            }

            return currentFragment!!
        }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onEventMainThread(event: VideoEvents.OpenVideoGroup) {
        val localVideoFragment = LocalVideoFragment()
        val bundle = Bundle()
        bundle.putSerializable(VideoGroup::class.java.simpleName, event.videoGroup)
        localVideoFragment.arguments = bundle
        supportFragmentManager
                .beginTransaction()
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                .hide(currentFragment)
                .add(R.id.fragment_container, localVideoFragment)
                .addToBackStack(null)
                .commit()
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onEventMainThread(event: VideoEvents.PlayLocalVideo) {
//        JCVideoPlayerStandard.startFullscreen(this, JCVideoPlayerStandard::class.java,
//                event.videoInfo.path, event.videoInfo.displayName)
                val intent = Intent(this, VideoPlayActivity::class.java);
                intent.putExtra(VideoInfo::class.java.getSimpleName(), event.videoInfo);
                startActivity(intent);
    }
}
