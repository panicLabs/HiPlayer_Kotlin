package id.panislabs.hiplayer.ui.activities

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.Toast
import fm.jiecao.jcvideoplayer_lib.JCVideoPlayerStandard
import id.paniclabs.hiplayer.R
import id.panislabs.hiplayer.models.VideoInfo


/**
 * @author      paniclabs.
 * @created     on 1/19/17.
 * @email       panic.inc.dev@gmail.com
 */

class VideoPlayActivity : AppCompatActivity() {

    var  mJcVideoPlayerStandard:JCVideoPlayerStandard? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.player_activity)

        var mVideoInfo = intent.extras.getSerializable(VideoInfo::class.java.simpleName)
        if(mVideoInfo == null){
            Toast.makeText(thism)
        }
        mJcVideoPlayerStandard = (findViewById(R.id.jc_video) as JCVideoPlayerStandard)

    }
}