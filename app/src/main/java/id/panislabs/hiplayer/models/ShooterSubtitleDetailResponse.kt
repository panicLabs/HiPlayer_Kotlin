package id.panislabs.hiplayer.models

/**
 * @author      paniclabs.
 * @created     on 2017/1/14.
 * @email       panic.inc.dev@gmail.com
 */
class ShooterSubtitleDetailResponse {

    var status: Int = 0
    var sub: SubBean? = null

    class SubBean {
        var result: String? = null
        var action: String? = null
        var subs: MutableList<SubBean.SubsBean>? = null

        class SubsBean {
            var filename: String? = null
            var native_name: String? = null
            var size: Int = 0
            var producer: SubBean.SubsBean.ProducerBean? = null
            var down_count: Int = 0
            var view_count: Int = 0
            var subtype: String? = null
            var videoname: String? = null
            var upload_time: String? = null
            var vote_score: Int = 0
            var url: String? = null
            var title: String? = null
            var id: Int = 0
            var lang: SubBean.SubsBean.LangBean? = null
            var filelist: MutableList<SubBean.SubsBean.FilelistBean>? = null

            class ProducerBean {
                var producer: String? = null
                var verifier: String? = null
                var source: String? = null
                var uploader: String? = null
            }

            class LangBean {
                var desc: String? = null

                var langlist: SubBean.SubsBean.LangBean.LanglistBean? = null

                class LanglistBean {
                    var isLangdou: Boolean = false
                    var isLangchs: Boolean = false
                    var isLangoth: Boolean = false
                }
            }

            class FilelistBean {
                var url: String? = null
                var f: String? = null
                var s: String? = null
            }
        }
    }
}
