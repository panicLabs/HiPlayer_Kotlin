package id.panislabs.hiplayer.models

/**
 * @author      paniclabs.
 * *
 * @created     on 2017/1/14.
 * *
 * @email       panic.inc.dev@gmail.com
 */
class ShooterSubtitleResponse {

    var status: Int = 0
    var sub: SubBean? = null

    class SubBean {
        var result: String? = null
        var action: String? = null
        var keyword: String? = null

        var subs: List<SubsBean>? = null

        class SubsBean {
            var native_name: String? = null
            var videoname: String? = null
            var revision: Int = 0
            var release_site: String? = null
            var upload_time: String? = null
            var vote_score: String? = null
            var id: String? = null
            var subtype: String? = null
            var lang: LangBean? = null

            class LangBean {
                var desc: String? = null
                var langlist: LanglistBean? = null

                class LanglistBean {
                    var isLangdou: Boolean = false
                }
            }
        }
    }
}
