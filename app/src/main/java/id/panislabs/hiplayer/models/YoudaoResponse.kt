package id.panislabs.hiplayer.models

import com.google.gson.annotations.SerializedName

/**
 * @author      paniclabs.
 * @created     on 2017/1/14.
 * @email       panic.inc.dev@gmail.com
 */
class YoudaoResponse {

    var basic: BasicBean? = null
    var query: String? = null
    var errorCode: Int = 0
    var translation: List<String>? = null
    var web: List<WebBean>? = null

    class BasicBean {
        var explains: List<String>? = null
        @SerializedName("us-phonetic")
        var us_phonetic: String? = null
        var phonetic: String? = null
        @SerializedName("uk-phonetic")
        var uk_phonetic: String? = null
    }

    class WebBean {
        var key: String? = null
        var value: List<String>? = null
    }
}
