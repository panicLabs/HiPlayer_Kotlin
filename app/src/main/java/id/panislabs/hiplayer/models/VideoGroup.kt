package id.panislabs.hiplayer.models

import java.io.Serializable

class VideoGroup : Serializable {
    var videoInfoList: MutableList<VideoInfo>? = null
    var groupName: String? = null
}
