package id.panislabs.hiplayer.models

import org.simpleframework.xml.Attribute
import org.simpleframework.xml.Element
import org.simpleframework.xml.ElementList
import org.simpleframework.xml.Root

/**
 * @author      paniclabs.
 * @created     on 2017/1/14.
 * @email       panic.inc.dev@gmail.com
 */
@Root(name = "dict", strict = false)
class ICibaResponse {
    @Attribute(name = "num", required = false)
    var num: String? = null
    @Attribute(name = "id", required = false)
    var id: String? = null
    @Attribute(name = "name", required = false)
    var name: String? = null
    @Element(name = "key")
    var key: String? = null
    @ElementList(inline = true, entry = "ps", required = false)
    var ps: List<String>? = null
    @ElementList(inline = true, entry = "pron", required = false)
    var pron: List<String>? = null
    @ElementList(inline = true, entry = "pos", required = false)
    var pos: List<String>? = null
    @ElementList(inline = true, entry = "acceptation", required = false)
    var acceptation: List<String>? = null
    @ElementList(inline = true, entry = "sent")
    var sent: List<SentBean>? = null

    @Element(name = "sent")
    class SentBean {
        @Element(name = "orig")
        var orig: String? = null
        @Element(name = "trans")
        var trans: String? = null
    }
}
