package id.panislabs.hiplayer.models

import java.io.Serializable

/**
 * @author      paniclabs.
 * @created     on 2017/1/14.
 * @email       panic.inc.dev@gmail.com
 */
class VideoInfo : Serializable {
    var title: String? = null
    var path: String? = null
    var size: Long = 0
    var thumbPath: String? = null
    var duration: Int = 0
    var folderPath: String? = null
    var displayName: String? = null
}
